// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDAPZHWzQ_Am372YS3MyQc2eTf4oyfz-t8',
    authDomain: 'pmc-enterprises.firebaseapp.com',
    databaseURL: 'https://pmc-enterprises.firebaseio.com',
    projectId: 'pmc-enterprises',
    storageBucket: 'pmc-enterprises.appspot.com',
    messagingSenderId: '570442790332',
    appId: '1:570442790332:web:a75fdc91839e315554a7b9',
    measurementId: 'G-4CFDX2TZ26'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
