import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  constructor(private db: AngularFirestore, private snackBar: MatSnackBar) { }

  saveMessage(value: any) {
    const id = this.db.createId();
    this.db.collection('messages').doc(id).set(value).then(
      _ => this.snackBar.open('Message Sent Successfully!', 'Ok', {duration: 5000, panelClass: ['green-snackbar']})
    );
  }
}
