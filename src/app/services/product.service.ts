import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Item } from '../models/item.model';
import { map, tap, first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db: AngularFirestore) {
  }

  public getAllProducts(): Observable<any> {
    return this.db.collection('products')
      .snapshotChanges()
      .pipe(
        map(snaps => {
          return snaps.map(snap => {
            return {
              id: snap.payload.doc.id,
              ...snap.payload.doc.data() as Item
            };
          });
        }),
        first()
      );
  }

  public getProductByName(name: string): Observable<Item[]> {
    return this.db.collection(
      'products',
      ref => ref.where('tags', 'array-contains', name))
      .snapshotChanges()
      .pipe(
        map(snaps => {
          return snaps.map(snap => {
            return {
              id: snap.payload.doc.id,
              ...snap.payload.doc.data() as Item
            };
          });
        }),
        tap(console.log),
        first()
      );
  }
}

