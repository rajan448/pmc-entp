import { BreakpointObserver } from '@angular/cdk/layout';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');

  constructor(private breakpointObserver: BreakpointObserver) {
  }
}
