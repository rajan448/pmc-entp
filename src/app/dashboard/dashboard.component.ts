import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  WA_URL = 'https://api.whatsapp.com/send?phone=919213132677';
  FB_URL = 'https://www.facebook.com/pages/category/App-Page/Pmc-enterprises-116231696449622/';
  EMAIL = 'mailto:pmc.entp@gmail.com';
  constructor() { }

  ngOnInit(): void {
  }

  navigateTo(appName: string): void {
    if (appName === 'mail') {
      window.location.href = this.EMAIL;
    }
    if (appName === 'wa') {
      window.open(this.WA_URL);
    }
    if (appName === 'fb') {
      window.open(this.FB_URL);
    }
  }
}
