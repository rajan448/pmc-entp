export interface Item {
  id: number;
  name: string;
  imageUrl: string;
  details: string;
  startingPrice: number;
}
