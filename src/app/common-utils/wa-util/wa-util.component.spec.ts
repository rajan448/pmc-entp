import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WAUtilComponent } from './wa-util.component';

describe('WAUtilComponent', () => {
  let component: WAUtilComponent;
  let fixture: ComponentFixture<WAUtilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WAUtilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WAUtilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
