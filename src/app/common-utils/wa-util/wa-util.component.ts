import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wa-util',
  templateUrl: './wa-util.component.html',
  styleUrls: ['./wa-util.component.scss']
})
export class WAUtilComponent implements OnInit {

  URL = 'https://api.whatsapp.com/send?phone=91';
  number;
  invalidNumber = false;

  constructor() { }

  ngOnInit(): void {
  }

  sendMessage(): void {
    if (this.number && this.number.toString().length === 10) {
      window.open(this.URL + this.number);

      this.number = '';
    } else {
      this.invalidNumber = true;
    }
  }
  onFocus() {
    this.invalidNumber = false;
  }

}
